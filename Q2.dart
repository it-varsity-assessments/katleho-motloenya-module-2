class MTNApps {
  String name;
  String year;

  MTNApps(this.name, this.year);
}
final wApps = [
    new MTNApps("FNB Banking App", "2012"),
    new MTNApps("SnapScan", "2013"),
    new MTNApps("Live Inspect", "2014"),
    new MTNApps("WumDrop", "2015"),
    new MTNApps("Domestly", "2016"),
    new MTNApps("Shyft", "2017"),
    new MTNApps("Khula", "2018"),
    new MTNApps("Naked Insurance", "2019"),
    new MTNApps("EasyEquities", "2020"),
    new MTNApps("Ambani Afrika", "2021")
  ];
String winningApps() {
  wApps.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
  String res = "";
  res += "a)\n";
  for (int i = 0; i < wApps.length; i++) {
    res += "Name: " + wApps[i].name + " - Year: " + wApps[i].year + "\n";
  }

  res += "\nb)";
  for (int i = 0; i < wApps.length; i++) {
    if (wApps[i].year == "2017") {
      res += "2017 winning app is " + wApps[i].name;
    } else if (wApps[i].year == "2018") {
      res += "\n2018 winning app is " + wApps[i].name + "\n";
    }
  }
  res += "\n\nc)\nTotal number of apps is " + (wApps.length).toString() + "\n";

  return res;
}


void main() {
  print(winningApps());
}
