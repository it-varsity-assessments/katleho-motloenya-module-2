import 'dart:io';

class AOTY {
  String appName;
  String appCategory;
  String appDeveloper;
  String appYear;

  AOTY(this.appName, this.appCategory, this.appDeveloper, this.appYear);

  String trans() {
    return appName.toUpperCase();
  }
}

void main() {
  //print(myData());
  //print(winningApps());
  print("Please enter app name");
  String? name = stdin.readLineSync();
  print("Please enter app category/sector");
  String? cat = stdin.readLineSync();
  print("Please enter app developer");
  String? dev = stdin.readLineSync();
  print("Please enter the year the app won the MTN Business App of the Year Awards");
  String? won = stdin.readLineSync();
  AOTY aoty = new AOTY(name!, cat!, dev!, won!);
  print("\na)\nApp Name: " +
      aoty.appName +
      "\nApp Category: " +
      aoty.appCategory +
      "\nApp Developer: " +
      aoty.appDeveloper +
      "\nApp won the MTN Business App of the Year Awards: " +
      aoty.appYear);
  print("\n\nb)\n" + aoty.trans() + "\n");
}
