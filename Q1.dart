import 'dart:io';

String myData() {
  print("All inputs are required\nEnter your name: ");
  String? name = stdin.readLineSync();
  print("Enter your favourite app: ");
  String? favApp = stdin.readLineSync();
  print("Enter your city: ");
  String? city = stdin.readLineSync();
  if (name == "" || favApp == "" || city == "") {
    return myData();
  } else {
    return "My name is $name \nMy favourite app is $favApp \nMy city is $city";
  }
}

void main() {
  print(myData());
}
